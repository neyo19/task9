const TodoItem = ({ id, title, completed, onChange, onDelete }) => {
    let titleClasses = "black-text";
    let deleteBtnClasses = "btn-flat red-text";

    const handleChangeCheckbox = (e) => {
        const { checked, value } = e.target;

        onChange(+value, checked);
    }

    const handleClickDeleteButton = () => {
        if (!completed) {
            onDelete(id);
        }
    }

    if (completed) {
        titleClasses += " line-through";
        deleteBtnClasses += " disabled";
    }

    return (<li className="collection-item">
        <label>
            <input
                type="checkbox"
                checked={completed}
                value={id}
                onChange={handleChangeCheckbox}
            />
            <span className={titleClasses}>{title}</span>
        </label>
        <div className="secondary-content">
            <i style={{color: 'red'}}> {`Id:${id+1}`} </i>
            <button
                className={deleteBtnClasses}
                onClick={handleClickDeleteButton}
            >
                <i className="material-icons">delete</i>
            </button>
        </div>
    </li>);


}

export default TodoItem;
